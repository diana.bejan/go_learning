![gopher logo](https://ctl.s6img.com/society6/img/MLcfOXaLRx86k4jqrGSZIcZROB8/w_700/prints/~artwork/s6-original-art-uploads/society6/uploads/misc/62485a7e59df4b388fe446bbe00ad452/~~/golang-gopher-wizard-prints.jpg) Go Developer Path
==============================================================

![gopher swim](https://golang.org/doc/gopher/appenginegopher.jpg) Tier 0

---------------------------------------------

|------|
| Take the [tour](https://tour.golang.org/welcome/1)| 
| Read [How to Write Go Code](https://golang.org/doc/code.html)|
| Read [Effective Go](https://golang.org/doc/effective_go.html) |
| Study Package [io](https://golang.org/pkg/io)|
| Study Package [ioutil](https://golang.org/pkg/io/ioutil) |
| Study Package [bufio](https://golang.org/pkg/bufio) |
| Study Package [strings](https://golang.org/pkg/strings) |
| Study Package [bytes](https://golang.org/pkg/bytes) ||
| Study Package [os](https://golang.org/pkg/os) ||
| Study Package [flag](https://golang.org/pkg/flag) |
| Study Package [http](https://golang.org/pkg/net/http) |
| Study Package [httputil](https://golang.org/pkg/net/http/httputil) |
| Study Package [httptest](https://golang.org/pkg/net/http/httptest) |
| Study Package [json](https://golang.org/pkg/encoding/json) |
| Study Package [context](https://golang.org/pkg/context) |
| Study Package [errors](https://golang.org/pkg/errors) |
| Study Package [fmt](https://golang.org/pkg/fmt) |
| Study Package [time](https://golang.org/pkg/time) |
| Study Package [sync](https://golang.org/pkg/sync) |
| Study Package [testing](http://golang.org/pkg/testing) |
| Read [The Book](https://www.gopl.io/) |
| Read [Why does Go not have assertions?](http://golang.org/doc/faq#assertions) |
| Read [Where is my favorite helper function for testing?](http://golang.org/doc/faq#testing_framework) |
| Watch [Testing Techniques](https://www.youtube.com/watch?v=ndmB0bj7eyw) by Andrew Gerrand[^ag] ([Slides](https://talks.golang.org/2014/testing.slide#1)) |
| Read [TableDrivenTests](https://github.com/golang/go/wiki/TableDrivenTests) from the [Go Wiki](https://github.com/golang/go/wiki) |
| Read [Using Subtests and Sub-benchmarks](https://blog.golang.org/subtests)|
| Watch [Go Proverbs](https://www.youtube.com/watch?v=PAAkCSZUG1c) by Rob Pike[^rp] ([Link](https://go-proverbs.github.io/)) |
| Read [Slice Tricks](https://github.com/golang/go/wiki/SliceTricks) from the [Go Wiki](https://github.com/golang/go/wiki) |

![gopher helmet](static/img/gopherhelmet_64.png) Tier 1

-------------------------------------------------

| Task |
|--------|
| Read [code review comments](https://github.com/golang/go/wiki/CodeReviewComments)|
| Read [idiomatic go](https://dmitri.shuralyov.com/idiomatic-go)|☐|☐|☐|🗹|☐|
| Read [Go, without package scoped variables](https://dave.cheney.net/2017/06/11/go-without-package-scoped-variables) by Dave Cheney[^dc]||
| Read [50 Shades of Go](http://devs.cloudimmunity.com/gotchas-and-common-mistakes-in-go-golang/index.html)|
| Read [The Go type system for newcomers](https://rakyll.org/typesystem) by Jaana B. Dogan[^jd]|
| Read [Go best practices, six years in](https://peter.bourgon.org/go-best-practices-2016) by Peter Bourgon[^pb]|
| Read [A theory of modern Go](https://peter.bourgon.org/blog/2017/06/09/theory-of-modern-go.html) by Peter Bourgon[^pb]|
| Read [Go for Industrial Programming](https://peter.bourgon.org/go-for-industrial-programming/) by Peter Bourgon[^pb]|
| Watch [Go best practices](https://www.youtube.com/watch?v=MzTcsI6tn-0) by Ashley McNamara[^am] and Brian Ketelsen[^bk]||
| Watch [Understanding Go Interfaces](https://www.youtube.com/watch?v=F4wUrj6pmSI) by Francesc Campoy[^fc] ([Slides](https://speakerdeck.com/campoy/understanding-the-interface)) |
| Watch [What's in a name](https://www.youtube.com/watch?v=sFUSP8Au_PE) by Andrew Gerrand[^ag] ([Slides](https://talks.golang.org/2014/names.slide))  |
| Watch [Twelve Go Best Practices](https://www.youtube.com/watch?v=8D3Vmm1BGoY)  by Francesc Campoy[^fc] ([Slides](https://talks.golang.org/2013/bestpractices.slide#2)) |
| Read [Style guideline for Go packages](https://rakyll.org/style-packages) by Jaana B. Dogan[^jd] |☐|☐|☐|🗹|☐|

![gopher running](https://golang.org/doc/gopher/biplane.jpg) Tier 2

------------------------
| Task |
|--------|
| Watch [Don't just check errors, handle them gracefully](https://dave.cheney.net/2016/04/27/dont-just-check-errors-handle-them-gracefully) and/or read the [blogpost](https://dave.cheney.net/2016/04/27/dont-just-check-errors-handle-them-gracefully) by Dave Cheney[^dc] |
| Watch [SOLID Go Design](https://www.youtube.com/watch?v=zzAdEt3xZ1M) and/or read the [blogpost](https://dave.cheney.net/2016/08/20/solid-go-design) by Dave Cheney[^dc] |
| Watch [Functional options for friendly APIs](https://www.youtube.com/watch?v=24lFtGHWxAQ) and/or read the [blogpost](https://dave.cheney.net/2016/04/27/dont-just-check-errors-handle-them-gracefully) by Dave Cheney[^dc] |

__... TBD__

----

[^rp]: **Rob Pike**
	* Twitter: [@rob_pike](https://twitter.com/rob_pike)
	* Blog: https://commandcenter.blogspot.com
	* GitHub: [robpike](https://github.com/robpike)
[^ag]: **Andrew Gerrand**
	* Blog: https://nf.wh3rd.net
	* GitHub: [adg](https://github.com/adg), [nf](https://github.com/nf)
[^dc]: **Dave Cheney**
	* Twitter: [@davecheney](https://twitter.com/davecheney)
	* Blog: https://dave.cheney.net
	* GitHub: [davecheney](https://github.com/davecheney)
[^jd]: **Jaana B. Dogan**
	* Twitter: [@rakyll](https://twitter.com/rakyll)
	* Blog: https://medium.com/@rakyll
	* GitHub: [rakyll](https://github.com/rakyll)
[^pb]: **Peter Bourgon**
	* Twitter: [@peterbourgon](https://twitter.com/peterbourgon)
	* Blog: https://peter.bourgon.org/blog
	* GitHub: [peterbourgon](https://github.com/peterbourgon)
[^fc]: **Francesc Campoy**
	* Twitter: [@francesc](https://twitter.com/francesc)
	* Speaker Deck: https://speakerdeck.com/campoy
	* Homepage/Blog: https://campoy.cat
	* Youtube Channel: [JustForFunc](https://www.youtube.com/channel/UC_BzFbxG2za3bp5NRRRXJSw)
	* GitHub: [campoy](https://github.com/campoy)
[^am]: **Ashley McNamara**
    * Twitter: [@@ashleymcnamara](https://twitter.com/ashleymcnamara)
    * GitHub: [ashleymcnamara](https://github.com/ashleymcnamara)
    * Blog: https://medium.com/@ashleymcnamara
[^bk]: **Brian Ketelsen**
    * Twitter: [@bketelsen](https://twitter.com/bketelsen)
    * GitHub: [bketelsen](https://github.com/bketelsen)
    * Blog: https://www.brianketelsen.com/
